#!/bin/sh

# YOU MUST HAVE SPACES TURNED OFF TO EDIT THIS FILE!!!
# ONLY USE TABS!!!!
#
# USE GVIM INSTEAD OF NEOVIM!!!
# (since my .gvimrc has tabs)
	
	#IMG:./icons/web.png	Librewolf	firejail librewolf --private

cat <<EOF | xmenu -r | sh &
Browsers
	Librewolf	firejail librewolf --private
	Brave	firejail brave --incognito
Tools
	Network Status	connman-gtk &
	Volume	qasmixer &
Terminals
	st		st -e fish &
	xterm	xterm &
File Manager	pcmanfm &

dmenu	dmenu_run &
Search	j4-dmenu-desktop &

Lock			~/.scripts/wm/lock.sh &
Suspend			~/.scripts/wm/locksuspend.sh &

Reboot			~/.scripts/wm/reboot.sh
Shutdown		~/.scripts/wm/shutdown.sh
EOF
